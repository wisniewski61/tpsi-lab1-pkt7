import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by student on 17.03.2017.
 * based on http://www.cs.put.poznan.pl/tpawlak/files/TPSI/TPSIServer.java
 */
public class HttpFileServer {
    private static String sharedFolder;
    public static void main(String args[]) throws IOException {
        System.out.println(args[0]);
        sharedFolder = args[0];
        int port = 8000;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new RootHandler());
        server.start();
    }

    private static class RootHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String uri = httpExchange.getRequestURI().toString();
            System.out.println(uri);
            String address = sharedFolder + uri;
            address = address.replace('/', '\\');
            address = address.replace("%20", " ");
            File file = new File(address);
            if(_isPathTraversal(file, sharedFolder, httpExchange))
                return;

            if(uri.equals("/")) {
                _listFiles(sharedFolder, httpExchange);
                return;
            }
            if(file.exists()) {
                // istnieje
                if(file.isDirectory()) {
                    _listFiles(address, httpExchange);
                } else if(file.isFile()) {
                    _sendFile(address, httpExchange);
                }//if-else
            } else {
                httpExchange.sendResponseHeaders(404, 0);
                OutputStream os = httpExchange.getResponseBody();
                //os.write(data);
                os.close();
            }
        }//handle

        private boolean _isPathTraversal(File file, String sharedFolder, HttpExchange httpExchange) throws IOException {
            String canonicalPath = file.getCanonicalPath();
            if(!canonicalPath.startsWith(sharedFolder)) {
                httpExchange.sendResponseHeaders(403, 0);
                OutputStream os = httpExchange.getResponseBody();
                //os.write(data);
                os.close();
                return true;
            }//if
            return false;
        }//_isPathTraversal()

        private void _sendFile(String address, HttpExchange httpExchange) throws IOException {
            Path path = Paths.get(address);
            byte[] data = Files.readAllBytes(path);
            httpExchange.sendResponseHeaders(200, data.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(data);
            os.close();
        }//_sendFile()

        private void _listFiles(String address, HttpExchange httpExchange) throws IOException {
            File file = new File(address);
            String response = "";
            File[] list = file.listFiles();
            for (File f :
                    list) {
                //if(f.isFile())
                    response += f.getName() + "\n";
            }//for
            System.out.println(response);
            httpExchange.getResponseHeaders().set("Content-Type", "text/plain; charset=utf-8");
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }//_listFiles()
    }
}
